#!/usr/bin/env python3
# pmda.py
# Polar Metagenomes Download and Assembly

__author__ = "Mikolaj Dziurzynski"

import argparse
import sys
import os
import shutil
import csv
import sqlite3
from sqlite3 import Error

import sqlalchemy as db
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

from multiprocessing import Pool
import subprocess
import time
import hashlib
import gzip


# constants
DATABASE_COLNAMES_WITH_DB_TYPES = {'polar_region': 'TEXT', 'lat': "REAL", 'long': "REAL", 'study_accession': "TEXT",
                                   'sample_accession': "TEXT", 'experiment_accession': "TEXT", 'run_accession': "TEXT",
                                   'submission_accession': "TEXT",
                                   'tax_id': "TEXT", 'scientific_name': "TEXT", 'instrument_platform': "TEXT",
                                   'instrument_model': "TEXT", 'library_layout': "TEXT",
                                   'library_strategy': "TEXT", 'library_source': "TEXT", 'library_selection': "TEXT",
                                   'read_count': "INTEGER",
                                   'base_count': "INTEGER", 'experiment_title': "TEXT", 'study_title': "TEXT",
                                   'fastq_bytes': "INTEGER", 'fastq_md5': "TEXT",
                                   'fastq_ftp': "TEXT", 'submitted_bytes': "TEXT", 'sample_title': "TEXT",
                                   'first_created': "TEXT", 'biome': "TEXT",
                                   "downloaded": "TEXT", "assembled": "TEXT", "prodigaled": "TEXT",
                                   "contig_number": "REAL", "n50": "REAL", "longest_contig_len": "REAL",
                                   "shortest_contig_len": "TEXT", "total_contig_len": "REAL", "stats": "TEXT",
                                   "error": "TEXT"}

INPUT_CSV_MANDATORY_COLNAMES = ['polar_region', 'lat', 'long', 'study_accession', \
'sample_accession', 'experiment_accession', 'run_accession', 'read_count', 'fastq_md5',\
'fastq_ftp', 'biome']


Base = declarative_base()

class Main_Table(Base):
    __tablename__ = 'polar_metagenomes_metadata'

    polar_region = Column(String)
    lat = Column(String)
    long = Column(String)
    study_accession = Column(String)
    sample_accession = Column(String)
    experiment_accession = Column(String)
    run_accession = Column(String, primary_key=True)
    submission_accession = Column(String)
    tax_id = Column(String)
    scientific_name = Column(String)
    instrument_platform = Column(String)
    instrument_model = Column(String)
    library_layout = Column(String)
    library_strategy = Column(String)
    library_source = Column(String)
    library_selection = Column(String)
    read_count = Column(String)
    base_count = Column(String)
    experiment_title = Column(String)
    study_title = Column(String)
    fastq_bytes = Column(String)
    fastq_md5 = Column(String)
    fastq_ftp = Column(String)
    submitted_bytes = Column(String)
    sample_title = Column(String)
    first_created = Column(String)
    biome = Column(String)
    downloaded = Column(String)
    downloaded_filepaths = Column(String)
    assembled = Column(String)
    prodigaled = Column(String)
    contig_number = Column(String)
    n50 = Column(String)
    longest_contig_len = Column(String)
    shortest_contig_len = Column(String)
    total_contig_len = Column(String)
    stats = Column(String)
    error = Column(String)


def parse_opts():
    """Define and parse options"""
    parser = argparse.ArgumentParser(description="""Polar Metagenomes Download and Assembly tool""")
    parser.add_argument("-i", "--input", type=str, help="Path to input csv")
    parser.add_argument("-da", "--download_and_assemble", action="store_true", help="Run download and assemble pipeline on the database.")


    if len(sys.argv) == 1:
        parser.print_help()
        print("\n")
        quit()

    args = parser.parse_args()
    return args


def database_setup_check():
    """
    Check if there is an SQLite database, if not then
    create one with necessary table and columns
    """

    db_exists = os.path.isfile('./polar_metagenomes_metadata.sqlite3')
    if not db_exists:
        print("No database found, creating a new database.")
    else:
        print("Database found.")

    engine = db.create_engine('sqlite:///polar_metagenomes_metadata.sqlite3')
    conn = engine.connect()
    meta = db.MetaData()
    polar_metagenomes_metadata = db.Table(
        'polar_metagenomes_metadata', meta,
        db.Column('polar_region', db.String()),
        db.Column('lat', db.Float()),
        db.Column('long', db.Float()),
        db.Column('study_accession', db.String()),
        db.Column('sample_accession', db.String()),
        db.Column('experiment_accession', db.String()),
        db.Column('run_accession', db.String()),
        db.Column('submission_accession', db.String()),
        db.Column('tax_id', db.String()),
        db.Column('scientific_name', db.String()),
        db.Column('instrument_platform', db.String()),
        db.Column('instrument_model', db.String()),
        db.Column('library_layout', db.String()),
        db.Column('library_strategy', db.String()),
        db.Column('library_source', db.String()),
        db.Column('library_selection', db.String()),
        db.Column('read_count', db.Float()),
        db.Column('base_count', db.Float()),
        db.Column('experiment_title', db.String()),
        db.Column('study_title', db.String()),
        db.Column('fastq_bytes', db.String()),
        db.Column('fastq_md5', db.String()),
        db.Column('fastq_ftp', db.String()),
        db.Column('submitted_bytes', db.String()),
        db.Column('sample_title', db.String()),
        db.Column('first_created', db.String()),
        db.Column('biome', db.String()),
        db.Column('downloaded', db.String()),
        db.Column('downloaded_filepaths', db.String()),
        db.Column('assembled', db.String()),
        db.Column('prodigaled', db.String()),
        db.Column('contig_number', db.Integer()),
        db.Column('n50', db.Integer()),
        db.Column('longest_contig_len', db.Integer()),
        db.Column('shortest_contig_len', db.Integer()),
        db.Column('total_contig_len', db.Integer()),
        db.Column('stats', db.String()),
        db.Column('error', db.String()),
    )

    meta.create_all(engine)

    print("Database setup finished.")
    return True


def get_db_cursor():
    engine = db.create_engine('sqlite:///polar_metagenomes_metadata.sqlite3')
    conn = engine.connect()
    meta = db.MetaData()
    main_table = db.Table('polar_metagenomes_metadata', meta, autoload=True, autoload_with=engine)

    return (conn, main_table)

    # row = {'polar_region': 1123, 'lat': '68.3532', 'long': '19.0477', 'study_accession': 'PRJNA518547', 'sample_accession': 'SAMN10864239', 'experiment_accession': 'SRX5358028', 'run_accession': 'SRR8556265', 'submission_accession': 'SRA847989', 'tax_id': '1799672', 'scientific_name': 'peat metagenome', 'instrument_platform': 'ILLUMINA', 'instrument_model': 'Illumina NovaSeq 6000', 'library_layout': 'PAIRED', 'library_strategy': 'WGS', 'library_source': 'METAGENOMIC', 'library_selection': 'other', 'read_count': '353315087', 'base_count': '106701156274', 'experiment_title': 'Illumina NovaSeq 6000 sequencing; Palsa_T0_2', 'study_title': 'Peat permafrost microbial communities from Stordalen Mire near Abisko, Sweden - Palsa_T0_2 metagenome', 'fastq_bytes': '24009489775;24623600654', 'fastq_md5': 'e40b85b242f2b511b5ab55005bbfc618;449315384109cce2f67f8d9326eb88f0', 'fastq_ftp': 'ftp.sra.ebi.ac.uk/vol1/fastq/SRR855/005/SRR8556265/SRR8556265_1.fastq.gz;ftp.sra.ebi.ac.uk/vol1/fastq/SRR855/005/SRR8556265/SRR8556265_2.fastq.gz', 'submitted_bytes': '', 'sample_title': 'Peat permafrost microbial communities from Stordalen Mire near Abisko, Sweden - Palsa_T0_2', 'first_created': '2019-02-12', 'biome': 'soil'}
    #
    # query = db.insert(polar_metagenomes_metadata).values(row)
    # ResultProxy = conn.execute(query)


def check_for_mandatory_col_names(fieldnames):
    print("Checking if mandatory colnames present")
    for colname in INPUT_CSV_MANDATORY_COLNAMES:
        if colname not in fieldnames:
            raise Exception("INPUT CSV MISSING MANDATORY COLUMN NAME: {0}.\nOTHER MISSING COLUMN NAMES: {1}".format(colname, set(INPUT_CSV_MANDATORY_COLNAMES)-set(fieldnames)))

    print("All mandatory column names present.")
    return


def create_update_database(input_path):
    """
    Creates and updates SQLite3 database based on input csv
    The input csv has to have the following column names:
    polar_region, lat, long, study_accession, sample_accession,
    experiment_accession, run_accession, read_count, fastq_md5, fastq_ftp, biome

    run_accession field is used as unique identifier
    """

    setup_success = database_setup_check()
    if not setup_success:
        print("Something went wrong on database checkup level.\nABORTING.")
        return


    conn, main_table = get_db_cursor()
    print("Connected to the database.")

    with open(input_path, 'r') as input_csv:
        reader = csv.DictReader(input_csv)
        check_for_mandatory_col_names(reader.fieldnames)
        for i, row in enumerate(reader):

            if row['run_accession'] == '':
                raise Exception("MISSING RUN ACCESSION! LINE: {0}.\nABORTING.".format(i+1))

            results = conn.execute(db.select([main_table]).where(main_table.columns.run_accession == row['run_accession']))
            if not list(results):
                row.update({'downloaded':'', 'assembled':'', 'prodigaled':'', 'stats':'', 'error':''})
                insert_results = conn.execute(db.insert(main_table).values(row))
                print("Added record with run accession {0}.".format(row['run_accession']))
            else:
                print("Run accession {0} already in the database - skipping.".format(row['run_accession']))

    print("\nData loaded. In order to process, run the script with -da flag.\n")
    return True


def md5_check(filenames, md5sums):
    print("\n[DOWNLOAD PIPELINE] Checking checksums.\n")
    for fname in filenames:
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        fname_md5sum = hash_md5.hexdigest()

        try:
            md5sums.remove(fname_md5sum)
        except ValueError:
            return False

    print("[DOWNLOAD PIPELINE] Checksums correct.\n")
    return True

def wget_runner(fastq_urls):
    filenames = []
    for url in fastq_urls:
        status = subprocess.run(['wget', '-v', url, '-P', 'raw_data'])
        if status.returncode != 0:
            return False
        filenames.append('raw_data/' + url.split('/')[-1])

    return filenames


def calculate_n50(list_of_contig_lens):
    n = sum(list_of_contig_lens) // 2
    contig_len_sum = 0
    for contig_len in list_of_contig_lens:
        contig_len_sum += contig_len
        if contig_len_sum >= n:
            return contig_len


def download_pipeline(record):

    conn, main_table = get_db_cursor()
    session = Session(bind=conn)

    record_zip = zip(['run_accession','library_layout', 'fastq_md5', 'fastq_ftp'], record)
    record_dict = dict(record_zip)

    print("[DOWNLOAD PIPELINE] Downloading {0}".format(record_dict['run_accession']))

    # read library layout, try to get file paths - 2 for PE and 1 for SE
    fastq_urls = []
    for url in record_dict['fastq_ftp'].split(';'):
        if record_dict['library_layout'] == 'SINGLE':
            fastq_urls.append(url)
            break
        elif record_dict['library_layout'] == 'PAIRED':
            # sometimes there are additional files with interwined reads
            if url[-11:] == '_1.fastq.gz' or url[-11:] == '_2.fastq.gz':
                fastq_urls.append(url)

    #check if correct number of urls
    if (record_dict['library_layout'] == "SINGLE" and len(fastq_urls) != 1) or (record_dict['library_layout'] == "PAIRED" and len(fastq_urls) != 2):
        print("[DOWNLOAD PIPELINE] Wrong number of urls, adding to error column and continuing.\n")
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
        record_to_update.error = "[DOWNLOAD PIPELINE] wrong number of urls after parsing"
        session.commit()
        return

    # run download
    filenames = wget_runner(fastq_urls)
    if isinstance(filenames, bool):
        print("[DOWNLOAD PIPELINE] Wrong number of urls, adding to error column and continuing.\n")
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
        record_to_update.error = "[DOWNLOAD PIPELINE] something wrong with download error code"
        session.commit()
        return
    else:
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
        record_to_update.downloaded_filepaths = ";".join(filenames)
        session.commit()

    # check md5sum
    md5sums = record_dict['fastq_md5'].split(';')
    if len(md5sums) == 0:
        print("[DOWNLOAD PIPELINE] No MD5 checksums, adding to error column and continuing.\n")
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
        record_to_update.error = "[DOWNLOAD PIPELINE] No MD5 checksums"
        session.commit()
        return

    md5sums_res = md5_check(filenames, md5sums)
    if not md5sums_res:
        print("[DOWNLOAD PIPELINE] Wrong MD5 sums, adding to error column and continuing.\n")
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
        record_to_update.error = "[DOWNLOAD PIPELINE] Wrong MD5 checksums"
        session.commit()
        return

    # when download finished, update the database-  
    record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
    record_to_update.downloaded = "TRUE"
    session.commit()

    session.close()

    print("[DOWNLOAD PIPELINE] Finished downloading {0}".format(record_dict['run_accession']))

    return True


def assembly_pipeline():
    """
    Read data from the database and run fastp and megahit assembly.
    After assembly remove raw data files and assembly tmp files.
    Leave only final.contigs.fa and compress it.

    Running in while loop, until no more files with
    empty 'downloaded' and 'error' columns 
    """
    print("[ASSEMBLY PIPELINE] Starting ASSEMBLY PIPELINE")

    conn, main_table = get_db_cursor()
    session = Session(bind=conn)

    records_waiting = session.query(Main_Table).filter_by(error='').filter_by(assembled='').all()

    megahit_out = os.path.isdir("megahit_out")
    if not megahit_out:
        os.makedirs("megahit_out")

    cleaned_data = os.path.isdir("cleaned_data")
    if cleaned_data:
        shutil.rmtree("cleaned_data")
    os.makedirs("cleaned_data")

    while len(records_waiting) > 0:

        record = session.query(Main_Table.run_accession, Main_Table.library_layout, Main_Table.downloaded_filepaths).filter_by(downloaded='TRUE').filter_by(error='').filter_by(assembled='').first()
        
        if not record:
            # wait 1 minut before trying to find another record to process
            print("\n[ASSEMBLY PIPELINE] Waiting 60 seconds for records to process")
            time.sleep(60)
            continue

        record_zip = zip(['run_accession', 'library_layout', 'downloaded_filepaths'], record)
        record_dict = dict(record_zip)
        record_dict['downloaded_filepaths'] = record_dict['downloaded_filepaths'].split(';')

        # run fastp
        print("\n[ASSEMBLY PIPELINE] Running fastp on {0}.\n".format(record_dict['run_accession']))
        if record_dict['library_layout'] == 'SINGLE':
            cmd = "fastp -i {0} -o cleaned_data/{1}_cleaned.fastq.gz -w 4 -j {1}_fastp.json".format(record_dict['downloaded_filepaths'][0], record_dict['run_accession'])
            status = subprocess.run(cmd.split(' '))
            if status.returncode != 0:
                print("[ASSEMBLY PIPELINE] Some problems with fastp.\n")
                record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
                record_to_update.error = "[ASSEMBLY PIPELINE] Some problems with fastp."
                session.commit()
                continue

        elif record_dict['library_layout'] == 'PAIRED':
            cmd = "fastp -i {0} -o cleaned_data/{2}_cleaned_1.fastq.gz -I {1} -O cleaned_data/{2}_cleaned_2.fastq.gz -w 4 -j {2}_fastp.json".format(record_dict['downloaded_filepaths'][0], record_dict['downloaded_filepaths'][1], record_dict['run_accession'])
            status = subprocess.run(cmd.split(' '))
            if status.returncode != 0:
                print("[ASSEMBLY PIPELINE] Some problems with fastp.\n")
                record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
                record_to_update.error = "[ASSEMBLY PIPELINE] Some problems with fastp."
                session.commit()
                continue
        
        else:
            print("[ASSEMBLY PIPELINE] Non standard library layout {0}.\n".format(record_dict['run_accession']))
            record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
            record_to_update.error = "[ASSEMBLY PIPELINE] Non standard library layout"
            session.commit()
            continue

        # run megahit
        print("\n[ASSEMBLY PIPELINE] Running megahit on {0}.\n".format(record_dict['run_accession']))

        # check if there is already megahit subdir for this record
        megahit_record_output = os.path.isdir("megahit_out/{0}".format(record_dict['run_accession']))
        if megahit_record_output:
            shutil.rmtree("megahit_out/{0}".format(record_dict['run_accession']))

        if record_dict['library_layout'] == 'SINGLE':
            cleaned_path = "cleaned_data/{0}_cleaned.fastq.gz".format(record_dict['run_accession'])
            cmd = "docker run -v {0}:/workspace -w /workspace vout/megahit megahit -r {1} -t 50 -o megahit_out/{2} --out-prefix {2}".format(os.getcwd(), cleaned_path, record_dict['run_accession'])
            status = subprocess.run(cmd.split(' '))
            if status.returncode != 0:
                print("[ASSEMBLY PIPELINE] Some problems with megahit.\n")
                record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
                record_to_update.error = "Some problems with megahit."
                session.commit()
                continue

        elif record_dict['library_layout'] == 'PAIRED':
            cleaned_path_1 = "cleaned_data/{0}_cleaned_1.fastq.gz".format(record_dict['run_accession'])
            cleaned_path_2 = "cleaned_data/{0}_cleaned_2.fastq.gz".format(record_dict['run_accession'])
            cmd = "docker run -v {0}:/workspace -w /workspace vout/megahit megahit -1 {1} -2 {2} -t 50 -o megahit_out/{3} --out-prefix {3}".format(os.getcwd(), cleaned_path_1, cleaned_path_2, record_dict['run_accession'])
            status = subprocess.run(cmd.split(' '))
            if status.returncode != 0:
                print("[ASSEMBLY PIPELINE] Some problems with megahit.\n")
                record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
                record_to_update.error = "[ASSEMBLY PIPELINE] Some problems with megahit."
                session.commit()
                continue

        # remove megahit tmps and compress final.contigs and log
        os.remove("megahit_out/{0}/done".format(record_dict['run_accession']))
        os.remove("megahit_out/{0}/opts.txt".format(record_dict['run_accession']))
        shutil.rmtree("megahit_out/{0}/intermediate_contigs".format(record_dict['run_accession']))

        final_contigs = "megahit_out/{0}/{0}.contigs.fa".format(record_dict['run_accession'])
        status = subprocess.run(['gzip',final_contigs])
        if status.returncode != 0:
            print("[ASSEMBLY PIPELINE] Problems with compression after megahit: {0}.\n".format(record_dict['run_accession']))
            record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
            record_to_update.error = "[ASSEMBLY PIPELINE] Problems with compression after megahit."
            session.commit()
            continue

        # remove cleaned data
        if len(record_dict['downloaded_filepaths']) == 1:
            os.remove("cleaned_data/{0}_cleaned.fastq.gz".format(record_dict['run_accession']))
        else:
            os.remove("cleaned_data/{0}_cleaned_1.fastq.gz".format(record_dict['run_accession']))
            os.remove("cleaned_data/{0}_cleaned_2.fastq.gz".format(record_dict['run_accession']))

        # remove raw data
        for fpath in record_dict['downloaded_filepaths']:
            os.remove(fpath)

        # update database
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_dict['run_accession']).first()
        record_to_update.assembled = "TRUE"
        session.commit()

        # update waiting records
        records_waiting = session.query(Main_Table).filter_by(error='').filter_by(assembled='').all()        

    # cleanup
    shutil.rmtree('cleaned_data')
    print("\n[ASSEMBLY PIPELINE] Finished ASSEMBLY PIPELINE\n")
    return True


def stats_pipeline():
    """
    Read data from the database and then create and fill the following:
    1) contig_number
    2) N50
    3) longest_contig_len
    4) smallest_contig_len
    5) total_contig_len

    The function runs in a while loop, for as long as
    there still are files without the aforementioned
    columns filled.
    """
    print("[STATS PIPELINE] Starting STATS PIPELINE")

    # Create connection and session
    conn, main_table = get_db_cursor()
    session = Session(bind=conn)

    # Check for rows without stats
    while len(session.query(Main_Table).filter_by(error='').filter_by(downloaded='TRUE').filter_by(
        assembled='TRUE').filter_by(stats='').all()) > 0:

        record_accession_number = session.query(Main_Table.run_accession).filter_by(downloaded='TRUE').filter_by(
            error='').filter_by(assembled='TRUE').filter_by(stats='').first()

        # Check if a record number was read    
        if not record_accession_number:
            # wait 1 minut before trying to find another record to process
            print("\n[STATS PIPELINE] No records to fill. Waiting 60 seconds for records to process")
            time.sleep(60)
            continue

        record_accession_number = record_accession_number[0]

        path_to_contig_file = "./megahit_out/{0}/{0}.contigs.fa.gz".format(record_accession_number)

        try:
            with gzip.open(path_to_contig_file, 'rb') as f:
                lines = f.readlines()
                HEADERS = lines[::2]
                CONTIG_LENS = [int(re.findall(r'len=([0-9]*)', str(head))[0]) for head in HEADERS]
        except:
            print("[STATS PIPELINE] Cannot open megahit output.\n")
            record_to_update = session.query(Main_Table).filter_by(run_accession=record_accession_number).first()
            record_to_update.error = "[STATS PIPELINE] Cannot open megahit output."
            session.commit()
            continue

        # Get required parameters leveraging sort() method and using calculate_n50()
        CONTIG_LENS.sort()
        CONTIG_NO = len(CONTIG_LENS)
        LONGEST_CONTIG = CONTIG_LENS[-1]
        SHORTEST_CONTIG = CONTIG_LENS[0]
        TOTAL_LEN = sum(CONTIG_LENS)
        N50 = calculate_n50(CONTIG_LENS)

        # update database
        record_to_update = session.query(Main_Table).filter_by(run_accession=record_accession_number).first()
        record_to_update.contig_number = CONTIG_NO
        record_to_update.n50 = N50
        record_to_update.longest_contig_len = LONGEST_CONTIG
        record_to_update.shortest_contig_len = SHORTEST_CONTIG
        record_to_update.total_contig_len = TOTAL_LEN
        record_to_update.stats = 'TRUE'
        session.commit()

        # update waiting records
        unfilled_rows = session.query(Main_Table).filter_by(error='').filter_by(contig_number='').filter_by(
            n50='').filter_by(longest_contig_len='').filter_by(shortest_contig_len='').filter_by(
            total_contig_len='').all()

        print("\n[STATS PIPELINE] Updated stats for {}\n".format(record_accession_number))

    session.close()
    print("\n[STATS PIPELINE] Finished STATS PIPELINE\n")
    return True


def pipeline_starter(input_data):

    if input_data['variant'] == 'download':
        download_pipeline(input_data['record'])

    elif input_data['variant'] == 'assembly':
    # fastp QC and megahit assebbly - remember that the raw files are in raw_data diretory
        assembly_pipeline()

    elif input_data['variant'] == 'stats':
        stats_pipeline()

    # elif variant == 'prodigal':
    #     prodigal_pipeline()

    else:
        raise Exception("WRONG PIPELINE VARIANT!\nABORTING.")


def prepare_process_queue():
    conn, main_table = get_db_cursor()
    session = Session(bind=conn)
    # get all reacords with empty download field
    to_download = session.query(Main_Table.run_accession, Main_Table.library_layout, Main_Table.fastq_md5, Main_Table.fastq_ftp).filter_by(downloaded='').filter_by(error='').all()

    process_queue = [{'variant': 'assembly'}, {'variant': 'stats'}]

    for record in to_download:
        process_queue.append({'variant': 'download', 'record': record})

    return process_queue


def main():
    args = parse_opts()

    if args.input:
        create_update_database(args.input)

    elif args.download_and_assemble:
        print("Starting download and assemble pipeline.\n")
        
        process_queue = prepare_process_queue()

        with Pool(5) as p:
            p.map(pipeline_starter, process_queue)

    print("Thats all for now. Bye")


if __name__ == "__main__":
    main()
